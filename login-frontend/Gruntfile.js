'use strict';
var fs = require('fs');

module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

// Project configuration.
    grunt.initConfig({
        connect: {
            server: {
                options: {
                    port: 5000,
                    base: 'dist',
                    livereload: true
                }
            }
        },

        clean: ['dist'],

        watch: {
            myDefault: {
                files: 'app/**/*',
                tasks: ['clean','copy' ],
                options: {
                    livereload: true
                }
            }
        },

        copy: {
            main: {
                files: [

                    // includes files within path and its sub-directories
                    {expand: true, cwd: './app', src: ['**/*'], dest: 'dist/'},
                    {expand: true, cwd: './bower_components', src: ['**/*'], dest: 'dist/bower_components'}
                ]
            }
        }
    });

    grunt.registerTask('default', 'playing with grunt', ['clean','copy' , 'connect', 'watch']);

};