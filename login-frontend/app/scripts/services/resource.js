/**
 * Created by shhab on 8/13/2016.
 */
(function () {
    "use strict";

    angular.module('login.services')
        .provider('resource', resourceProvider);


    function resourceProvider(){
        var _baseUrl = 'http://127.0.0.1:3333/api';
        var _resource = null;
        var _token = null;
        var _$q = null;
        var _makeConfig = function (method, params, token) {
            var config = {
                api: true,
                headers: {
                    Authorization: 'Token'+' '+ token
                },
                params: params,
                timeout: 30 * 1000, // 30sec
                transformResponse: function (data) {
                    return angular.fromJson(data);
                }
            };

            if (method === 'post' || method === 'put') {
                config.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
            }
            return config;
        };

        var _unwrap = function (httpPromise) {
            var dfd;
            dfd = _$q.defer();
            httpPromise.then(function (response) {
                dfd.resolve(response.data);

            }, function (response) {
                if (!response.data) {
                    response.data = {};
                }
                if (!response.status || response.status === -1) {
                    _.defaults(response.data, {message: 'Network error'});
                } else {
                    _.defaults(response.data, {message: 'Unknown error'});
                }
                dfd.reject(response.data);
            });
            return dfd.promise;
        };

        var _joinUrl = function (url1, url2) {
                url1 = url1[url1.length - 1] === '/' ? url1.substr(0, url1.length - 1) : url1;
                url2 = url2[0] === '/' ? url2.substr(1, url2.length - 1) : url2;
                return "" + url1 + "/" + url2;
        };

        this.$get = resourceProviderFactory;
        resourceProviderFactory.$inject = ['$http', '$q'];
        function resourceProviderFactory($http, $q){
            _$q = $q;
            _token = window.localStorage.getItem('token');
            _resource = {
                get: function (url, params) {
                    var config = _makeConfig('get', params, _token);
                    return _unwrap($http.get(_joinUrl(_baseUrl, url), config));
                },

                post: function (url, params, data) {
                    if (data == null) {
                        data = {};
                    }
                    var formData = [];
                    _.forEach(data, function (value, key) {
                        if (value !== null && angular.isDefined(value))
                            formData.push(encodeURIComponent(key) + "=" + encodeURIComponent(value));
                    });
                    var config = _makeConfig('post', params, _token);
                    return _unwrap($http.post(_joinUrl(_baseUrl, url), formData.join('&'), config));
                }
            };
            return _resource;
        }

    }


}).call();