/**
 * Created by shhab on 8/13/2016.
 */
(function () {
    "use strict";

    angular.module('login.services', []);
    angular.module('login.services')
        .factory('api', apiFactory);

    apiFactory.$inject = ['resource'];

    function apiFactory(resource){
        return {
            authentication: {
                login: function(credentials){
                    return resource.get('/login', credentials);
                }
            },
            profile: {
                getProfile: function(){
                    return resource.get('/secured_resource')
                }
            }
        };
    }

}).call();