/**
 * Created by shhab on 8/12/2016.
 */
(function(){
    'use strict';

    angular.module('login-app',[
        'ui.router',
        'login.controllers',
        'login.factories',
        'login.services'
    ])
        .config([
            '$stateProvider',
            '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider){
            $stateProvider
                .state('main',{
                    abstract: true
                })
                .state('main.login',{
                    url: '/login',
                    views: {
                        'content@':{
                            templateUrl: 'partials/login.html'
                        }
                    }
                })
                .state('main.profile',{
                    url: '/profile/:userId',
                    views: {
                        'content@': {
                            templateUrl: 'partials/profile.html'
                        }
                    }
                })
            ;
            $urlRouterProvider.otherwise('/login');

        }]);
}).call();