/**
 * Created by shhab on 8/13/2016.
 */

(function(){
    'use strict';

    angular.module('login.controllers', [])
        .controller('appController', appCtrl)
        .controller('loginController', loginCtrl)
        .controller('profileController', profileCtrl)
    ;

    function appCtrl(){

    }

    loginCtrl.$inject = ['$scope', 'loginModel'];
    function loginCtrl($scope, loginModel){
        $scope.loginModel = new loginModel();
    }

    profileCtrl.$inject = ['$scope', 'profileModel'];
    function profileCtrl($scope, profileModel){
        $scope.profileModel = new profileModel();
    }
}).call();
