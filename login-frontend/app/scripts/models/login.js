/**
 * Created by shhab on 8/13/2016.
 */
(function(){
    'use strict';

    angular.module('login.factories', []);
    angular.module('login.factories').factory('loginModel', loginModelFactory);

    loginModelFactory.$inject = ['$state', 'api'];
    function loginModelFactory($state, api){
        function loginModel(){
            this.username = null;
            this.password = null;
            this.login_error = false;
        }

        loginModel.prototype.login = function(){
            var self = this;
            var credentials = {
                usr: self.username,
                pw: self.password
            };
            self.login_error = false;
            return api.authentication.login(credentials)
                .then(function(result){
                    $state.go('main.profile');
                    window.localStorage.setItem('token', result['token']);
                }, function(){
                    self.login_error= true;
                })
            ;
        };

        return loginModel;
    }

}).call();