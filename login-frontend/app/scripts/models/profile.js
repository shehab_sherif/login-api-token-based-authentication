/**
 * Created by shhab on 8/13/2016.
 */
(function(){
    'use strict';

    angular.module('login.factories')
        .factory('profileModel', profileModelFactory);

    profileModelFactory.$inject = ['api'];
    function profileModelFactory(api){
        function profileModel(){
            this.user = api.profile.getProfile();
        }

        return profileModel;
    }

}).call();
