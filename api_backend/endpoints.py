from flask import Flask, request, json

app = Flask(__name__)

from api_backend.entities import User, Token, validate_user

def jsonify(data):
    import werkzeug.local
    import functools
    import flask.globals

    indent = 0
    request = werkzeug.local.LocalProxy(functools.partial(flask.globals._lookup_req_object, 'request'))

    if app.config['JSONIFY_PRETTYPRINT_REGULAR'] and not request.is_xhr:
        indent = 2

    return app.response_class(flask.json.dumps(data, indent=indent, cls=app.json_encoder),
                              mimetype='application/json')


@app.route('/api/login', methods=['POST'])
def login_end_point():
    loginuser = User.get_user(request.form.get('usr', None), request.form.get('pw', None))
    if loginuser:
        user_token = Token(userid=loginuser.id)
        return jsonify({'Token': user_token.token.decode('unicode_escape')})
    else:
        return jsonify({'message': 'User not found'})


@app.route('/api/secured_resource')
@validate_user
def secured(login_user):
    return jsonify({'user': login_user})
