import jwt

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from passlib.apps import custom_app_context
from sqlalchemy.orm import relationship
from functools import wraps
from sqlalchemy import create_engine
from flask import request, abort
from api_backend.api import config

Table = declarative_base()

class User(Table):
    __tablename__ = 'user'
    user_name = Column(String(100), nullable=False)
    hashed_password = Column(String(250), nullable=False) #don't be naive, save the password hashed
    id = Column(Integer, primary_key=True)

    @classmethod
    def get_user(cls,uname=None, pw=None, uid=None):
        db_session = sessionmaker(bind=engine)
        session = db_session()
        if (uname is None) & (pw is None):
            if uid is None:
                return None
            else:
                user = session.query(User).filter_by(id=uid).first()
                return user

        user_login = session.query(User).filter_by(user_name=uname).first()
        if not custom_app_context.verify(pw, user_login.hashed_password):
            return None
        user = user_login
        return user

    @staticmethod
    def hash_pasword(pw):
        #hash any password
        return custom_app_context.encrypt(pw)


class Token(Table):
    __tablename__ = 'token'
    user_id = Column(Integer, ForeignKey('user.id'))
    token = Column(String, nullable=False)
    user = relationship(User)
    id = Column(Integer, primary_key=True)

    def __init__(self, userid):
        self.user_id = userid
        self.token = self._generate_token()

    def _generate_token(self):
        self.token = jwt.encode({'userid': self.user_id}, config['secretKey'], algorithm=config['jwtAlgorithm'])
        db_session = sessionmaker(bind=engine)
        token_session = db_session()
        token_session.add(self)
        token_session.commit()
        return self.token


def validate_user(func):

    @wraps(func)
    def check_token(*args, **kwargs):
        token = request.headers.get('session_token')
        if token is None:
            abort(401)
        user_id = jwt.decode(token, config['secretKey'], algorithm=config['jwtAlgorithm'])
        user = User.get_user(uid=user_id)
        if user is None:
            abort(401)
        kwargs.update(login_user=user)
        return func(*args, **kwargs)

    return check_token


engine = create_engine('sqlite:///users.db')
Table.metadata.create_all(engine)