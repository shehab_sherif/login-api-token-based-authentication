from api_backend.endpoints import app

config = {
    'baseUrl': '127.0.0.1',
    'port': 3333,
    'secretKey': 'login-api',
    'jwtAlgorithm': 'HS256'
}

if __name__ == '__main__':
    #run server here
    app.run(config['baseUrl'],config['port'],debug=True)